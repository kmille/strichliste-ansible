#!/bin/sh

HOSTNAME=$(hostname | cut -d"." -f1)
REMOTE='borg.cysec.de'

REPOSITORY=snackops@$REMOTE:$HOSTNAME
DATE=`date +%Y-%m-%d_%H:%M`

borg create                                    \
    $REPOSITORY::$DATE                         \
    /opt/strichliste                           \

borg prune -v $REPOSITORY --prefix '{hostname}-' \
    --keep-within=6m

