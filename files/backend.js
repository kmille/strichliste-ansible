module.exports = {
    host: '0.0.0.0',
    port: 8080,

    database: {
        type: 'SQLITE',
        options: {
            filename: '/opt/strichliste/data.sqlite'
        }
    },

    mqtt: {
        enabled: false,
        host: 'mqtt.w17.io',
        port: 1883,
        topics: {
            transactionValue: 'w17/prepaid/transaction/value'
        }
    },

    boundaries: {
        account: {
            upper: 500,
            lower: -250
        },
        transaction: {
            upper: 500,
            lower: -250
        }
    },

    logging: {
        active: true
    },

    caching: {
        enabled: false
    },

    metrics: {
        timespan: 30
    }
};
